package com.group6.projectjee.web.controllers.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseSuccess {

    private Boolean success;
    private String message;
    private float value;

    public ResponseSuccess(String message, float value){
        this.success = true;
        this.message = message;
        this.value = value;
    }

}

package com.group6.projectjee.web.controllers.post;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data

public class IngredientQuantityDto {
    @NotBlank(message = "name ne peut être vide")
    private String name;

    @Min(value = 1, message = "la quantité doit être supérieur à 0")
    @NotBlank(message = "quantity ne peut être vide")
    private float quantity;
}

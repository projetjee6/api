package com.group6.projectjee.web.controllers.responses;

import java.util.ArrayList;

public class ResponseSuccessRecalculate {

    private Boolean success;
    private String message;
    private ArrayList<?> value;
    private String message2;
    private ArrayList<?> value2;

    public ResponseSuccessRecalculate(String message, ArrayList<?> value, String message2, ArrayList<?> value2){
        this.success = true;
        this.message = message;
        this.value = value;
        this.message2 = message2;
        this.value2 = value2;
    }
}

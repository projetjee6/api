package com.group6.projectjee.web.controllers.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseFail {

    private Boolean success;
    private String message;
    private String reason;

    public ResponseFail(String reason){
        this.success = false;
        this.message = "Request failed";
        this.reason = reason;
    }
}

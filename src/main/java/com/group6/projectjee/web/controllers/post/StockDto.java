package com.group6.projectjee.web.controllers.post;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StockDto {

    @NotBlank(message = "name ne peut être vide")
    private String name;

    @NotNull(message = "un user id doit être fourni")
    private long user;
}

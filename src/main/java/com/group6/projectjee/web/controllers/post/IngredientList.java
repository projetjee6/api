package com.group6.projectjee.web.controllers.post;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IngredientList {
    @NotBlank(message = "Veuillez fournir une liste d'ingrédients non vide")
    String[] ingredients;
}
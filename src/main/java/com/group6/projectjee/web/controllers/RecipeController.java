package com.group6.projectjee.web.controllers;

import com.google.gson.Gson;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.use_case.exceptions.RecipeNotFoundException;
import com.group6.projectjee.use_case.services.IngredientService;
import com.group6.projectjee.use_case.services.RecipeIngredientService;
import com.group6.projectjee.use_case.services.RecipeService;
import com.group6.projectjee.use_case.services.RestService;
import com.group6.projectjee.web.controllers.post.*;
import com.group6.projectjee.web.controllers.responses.ResponseFail;
import com.group6.projectjee.web.controllers.responses.ResponseSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/recipe")
public class RecipeController {

    private final RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService, RestService restService, Gson g, RecipeIngredientService recipeIngredientService) {
        this.recipeService = recipeService;
    }

    /**
     * Insert a new recipe into Database
     * @param recipe : RecipeDto that contains the recipe we want to create
     * @return New Recipe
     */
    @PostMapping
    @ResponseStatus(CREATED)
    public ResponseEntity<?> create(@RequestBody @Valid RecipeDto recipe)  {
        try {

            RecipeEntity result = null;
            if (recipe.getIngredients() == null){
                result = this.recipeService.create(new RecipeEntity(recipe.getName()));
            } else {
                result = this.recipeService.create(new RecipeEntity(recipe.getName()));
                for (IngredientQuantityDto ingredient : recipe.getIngredients()
                ) {
                    recipeService.addIngredientToRecipe(new RecipeIngredientDto(recipe.getName(), ingredient.getName(), ingredient.getQuantity() ));
                }
            }
            return new ResponseEntity<>(result, CREATED) ;
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseFail(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all recipes from database
     * @return An iterable that contains all recipes from database
     */
    @GetMapping("")
    public Iterable<RecipeEntity> getRecipes() { return this.recipeService.getAllRecipe();}

    /**
     * Get a recipe from database if name exists in database
     * @param name : String recipe name we want to search
     * @return Response entity that contains the recipe if recipe exists
     * else return response fail
     */
    @GetMapping("/{name}")
    public RecipeEntity getByName(@PathVariable String name){
        return this.recipeService.findRecipeByName(name);
    }

    /**
     * Get a random recipe from database
     * @return a random recipe from database
     */
    @GetMapping("/random-recipe")
    public RecipeEntity getRandomRecipe(){
        return this.recipeService.getRandomRecipe();
    }

    /**
     * Determine if a recipe contains given ingredient
     * @param recipe_name : String recipe's name
     * @param ingredient_name : String ingredient's name
     * @return true if the recipe contains ingredient, else false
     */
    @GetMapping("/{recipe_name}/ingredient/{ingredient_name}")
    public ResponseEntity<?> hasIngredient(@PathVariable String recipe_name, @PathVariable String ingredient_name) {
        if(this.recipeService.hasIngredient(recipe_name, ingredient_name)) {
            return new ResponseEntity<>(new ResponseSuccess("Recipe " + recipe_name + " has " + ingredient_name, 1), HttpStatus.OK);
        }
        return new ResponseEntity<>(new ResponseSuccess("Recipe " + recipe_name + " dont has " + ingredient_name, 0), HttpStatus.OK);
    }

    /**
     * Get all ingredients from recipe
     * @param recipe_id : Long Recipe id
     * @return Response entity that contains all ingredients from recipe if recipe exists,
     * else return response fail
     */
    @GetMapping("/{recipe_id}/ingredient")
    public ResponseEntity<?> getAllIngredient(@PathVariable Long recipe_id) {
        try{
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("success", true);
            map.put("recipe", this.recipeService.findRecipeById(recipe_id).getName());
            map.put("ingredients", this.recipeService.getRecipeIngredients(recipe_id));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (RecipeNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Recipe not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Add an ingredient to a recipe
     * @param recipeIngredientDto object with the recipe name, the ingredient name and the quantity
     * @return success : Response entity to confirm transaction,
     * else return Response entity with a failure message
     */
    @PostMapping("/ingredient")
    public ResponseEntity<?> addIngredient(@RequestBody RecipeIngredientDto recipeIngredientDto) {
        try {
            this.recipeService.addIngredientToRecipe(recipeIngredientDto);
            return new ResponseEntity<>(new ResponseSuccess(
                    recipeIngredientDto.getIngredient_name()
                    + " ajouté avec succès à " + recipeIngredientDto.getRecipe_name(),
                    0),
                    HttpStatus.OK
            );
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseFail(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all recipes that match ingredients passed in parameter
     * @param ingredientList String[] List of ingredients to filter recipes by
     * @return success : Response Entity with an array of string containing filtered recipes name,
     * else return a failure message
     */
    @PostMapping("/ingredients")
    public ResponseEntity<?> getRecipeByIngredients(@RequestBody IngredientList ingredientList) {
        try {
            ArrayList<RecipeDto> recipes = this.recipeService.getRecipeByIngredients(ingredientList);
            return new ResponseEntity<>(recipes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseFail(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get price for a given recipe
     * @param recipe_id : Long recipe id
     * @return String that is equal to the price of the recipe if API works,
     * else return api is down
     */
    @GetMapping("/{recipe_id}/price")
    public String getIngredientPrices(@PathVariable Long recipe_id) { return this.recipeService.getIngredientsPrice(recipe_id); }

}

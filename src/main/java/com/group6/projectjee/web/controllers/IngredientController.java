package com.group6.projectjee.web.controllers;

import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.use_case.services.IngredientService;
import com.group6.projectjee.web.controllers.post.IngredientDto;
import com.group6.projectjee.web.controllers.responses.ResponseFail;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/ingredient")
public class  IngredientController {

    private final IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    /**
     * Insert a new Ingredient into database
     * @param ingredient : IngredientDto that contains ingredient's data
     * @return New Ingredient Entity
     */
    @PostMapping
    public ResponseEntity<?> create(@RequestBody @Valid IngredientDto ingredient) {
        try {
            IngredientEntity newIngredient = this.ingredientService.create(new IngredientEntity(ingredient.getName(), ingredient.getBaseUnit()));
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("success", true);
            map.put("added ingredient", newIngredient.getName());
            return new ResponseEntity<>(map, HttpStatus.CREATED);
        }catch (DataIntegrityViolationException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient already exists"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all ingredients from database
     * @return An iterable that contains all Ingredients from database
     */
    @GetMapping("")
    @NotFound
    public Iterable<IngredientEntity> getIngredients() { return this.ingredientService.getAllIngredients();}

    /**
     * Get an ingredient from database that has given name
     * @param name : String ingredient name
     * @return Ingredient entity if ingredient exists
     * else return null
     */
    @GetMapping("/{name}")
    @NotFound
    public IngredientEntity getIngredientByName(@PathVariable String name) {
        return this.ingredientService.findByName(name);
    }


}

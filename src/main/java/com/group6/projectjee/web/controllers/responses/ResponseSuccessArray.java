package com.group6.projectjee.web.controllers.responses;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class ResponseSuccessArray {

    private Boolean success;
    private String message;
    private ArrayList<?> value;

    public ResponseSuccessArray(String message, ArrayList<?> value){
        this.success = true;
        this.message = message;
        this.value = value;
    }

}

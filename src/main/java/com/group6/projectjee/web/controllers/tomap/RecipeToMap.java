package com.group6.projectjee.web.controllers.tomap;

import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class RecipeToMap {

    public static Map<String, Object> recalculateToMap(ArrayList<IngredientEntity> usedIngredients, ArrayList<IngredientEntity> ingredientEntityArrayList){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("used_ingredients", usedIngredients);
        map.put("ingredients_under_limit", ingredientEntityArrayList);
        return map;
    }

    public static Map<String, Object> availableRecipesToMap(ArrayList<RecipeEntity> recipeEntityList){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("recipes", recipeEntityList);
        return map;
    }
}

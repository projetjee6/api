package com.group6.projectjee.web.controllers.tomap;

import com.group6.projectjee.domain.models.Ingredient;
import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import io.swagger.models.auth.In;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class IngredientToMap {

    public static Map<String, Object> ingredientEntityQuantityToMap(IngredientEntity ingredientEntity, float quantity, BaseUnit unit){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredientEntity.getName());
        map.put("quantity", quantity);
        map.put("unit", unit);
        return map;
    }

    public static Map<String, Object> ingredientEntityListToMap(List<IngredientEntity> ingredientEntityList){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredients", ingredientEntityList);
        return map;
    }

    public static Map<String, Object> addedQuantityToMap(IngredientEntity ingredientEntity, float previousQuantity, float newQuantity){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredientEntity.getName());
        map.put("previous_quantity", previousQuantity);
        map.put("new_quantity", newQuantity);
        return map;
    }

    public static Map<String, Object> updatedLimitToMap(IngredientEntity ingredientEntity, float previousLimit, float newLimit){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredientEntity.getName());
        map.put("previous_limit", previousLimit);
        map.put("new_limit", newLimit);
        return map;
    }

    public static Map<String, Object> getLimitToMap(IngredientEntity ingredientEntity, float limit){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("success", true);
        map.put("ingredient", ingredientEntity.getName());
        map.put("limit", limit);
        return map;
    }

}

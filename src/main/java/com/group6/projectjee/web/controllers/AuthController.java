package com.group6.projectjee.web.controllers;

import com.group6.projectjee.domain.models.enums.ERole;
import com.group6.projectjee.infrastructure.persistence.entities.Role;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.use_case.exceptions.UserRoleNotDefinedException;
import com.group6.projectjee.web.controllers.post.LoginRequest;
import com.group6.projectjee.web.controllers.post.SignupRequest;
import com.group6.projectjee.use_case.services.response.JwtResponse;
import com.group6.projectjee.use_case.services.response.MessageResponse;
import com.group6.projectjee.infrastructure.persistence.dal.RoleDAL;
import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import com.group6.projectjee.web.jwt.JwtUtils;
import com.group6.projectjee.use_case.services.authent.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDAL userDAL;

    @Autowired
    RoleDAL roleDAL;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    /**
     * Allows user to log-in if information are corrects else forbids authentication
     * @param loginRequest : LoginRequest that contains all Login information
     * @return Response Entity that said if Authentication is successful or why it failed
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest ){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(   jwt,
                                                    userDetails.getId(),
                                                    userDetails.getUsername(),
                                                    userDetails.getEmail(),
                                                    roles));
    }

    /**
     * Create a new account if conditions are met
     * @param signUpRequest
     * @return
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userDAL.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userDAL.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        UserEntity userEntity = new UserEntity(signUpRequest.getFirstname(),
                signUpRequest.getLastname(),
                signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleDAL.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new UserRoleNotDefinedException());
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleDAL.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    default:
                        Role userRole = roleDAL.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        userEntity.setRoles(roles);
        userDAL.save(userEntity);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }


}

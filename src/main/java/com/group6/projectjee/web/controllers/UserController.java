package com.group6.projectjee.web.controllers;

import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.use_case.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    /**
     * Get all users from database
     * @return An iterable that contains all users
     */
    @GetMapping
    public Iterable<UserEntity> getUsers() { return this.userService.getAllUsers();}
}

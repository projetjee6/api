package com.group6.projectjee.web.controllers;

import com.group6.projectjee.infrastructure.persistence.entities.*;
import com.group6.projectjee.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.use_case.exceptions.RecipeNotFoundException;
import com.group6.projectjee.use_case.exceptions.UsernameNotFoundException;
import com.group6.projectjee.use_case.services.IngredientService;
import com.group6.projectjee.use_case.services.UserService;
import com.group6.projectjee.web.controllers.post.StockDto;
import com.group6.projectjee.web.controllers.responses.*;
import com.group6.projectjee.use_case.services.StockService;
import com.group6.projectjee.web.controllers.tomap.IngredientToMap;
import com.group6.projectjee.web.controllers.tomap.RecipeToMap;
import org.springframework.aop.AopInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/stock")
@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
public class StockController {

    private final StockService stockService;
    private final UserService userService;
    private final IngredientService ingredientService;

    @Autowired
    public StockController(StockService StockService, UserService userService, IngredientService ingredientService){
        this.stockService = StockService;
        this.userService = userService;
        this.ingredientService = ingredientService;
    }

    /**
     * Get all ingredients that user has
     * @param userId : Long user Id
     * @return success Response entity that contains all User's ingredients if user exists,
     * else Failed Response entity
     */
    @GetMapping("/{userId}/ingredient")
    public ResponseEntity<?> getUserIngredients(@PathVariable Long userId) {
        try{
            return new ResponseEntity<>(IngredientToMap.ingredientEntityListToMap(
                    stockService.getUserIngredients(userId)), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get an ingredient Quantity from user's stock
     * @param userId : user Id
     * @param ingredientId : ingredient Id
     * @return success Response entity that contains Ingredient's quantity possessed by user if user and ingredient exist,
     * else, return failed response
     */
    @GetMapping("/{userId}/ingredient/{ingredientId}/quantity")
    public ResponseEntity<?> getUserIngredientQuantity(@PathVariable Long userId, @PathVariable Long ingredientId) {
        try{
            return new ResponseEntity<>(IngredientToMap.ingredientEntityQuantityToMap(
                        this.ingredientService.findById(ingredientId),
                        this.stockService.getUserIngredientQuantity(userId, ingredientId),
                        this.ingredientService.findById(ingredientId).getBaseUnit()),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException ex){
            return new ResponseEntity<>(new ResponseFail("Ingredient does not exist"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Add Quantity to an ingredient in user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @param quantity : float Quantity added
     * @return Success response entity that contains the updated stock quantity for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PostMapping("/{userId}/ingredient/{ingredientId}/quantity/{quantity}")
    public ResponseEntity<?> addUserIngredientQuantity(@PathVariable Long userId, @PathVariable Long ingredientId, @PathVariable float quantity) {
        try{
            return new ResponseEntity<>(IngredientToMap.addedQuantityToMap(
                        this.ingredientService.findById(ingredientId),
                        this.stockService.getUserIngredientQuantity(userId, ingredientId),
                        this.stockService.addIngredientQuantity(userId, ingredientId, quantity)),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException ex){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update Ingredient quantity from user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @param quantity : float quantity to update
     * @return Success response entity that contains the updated stock quantity for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PatchMapping("/{userId}/ingredient/{ingredientId}/quantity/{quantity}")
    public ResponseEntity<?> updateIngredientQuantity(@PathVariable Long userId, @PathVariable Long ingredientId, @PathVariable Float quantity) {
        try{
            return new ResponseEntity<>(IngredientToMap.addedQuantityToMap(
                        this.ingredientService.findById(ingredientId),
                        this.stockService.getUserIngredientQuantity(userId, ingredientId),
                        this.stockService.updateIngredientQuantity(userId, ingredientId, quantity)),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update Ingredient limit for an ingredient from user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @param quantity : float Quantity to add
     * @return Success response entity that contains the updated limit in stock for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PatchMapping("/{userId}/ingredient/{ingredientId}/limit-quantity/{quantity}")
    public ResponseEntity<?> updateIngredientLimit(@PathVariable Long userId, @PathVariable Long ingredientId, @PathVariable Float quantity) {
        try{
            return new ResponseEntity<>(IngredientToMap.updatedLimitToMap(
                    this.ingredientService.findById(ingredientId),
                    this.stockService.getIngredientLimit(userId, ingredientId),
                    this.stockService.updateIngredientLimit(userId, ingredientId, quantity)
            ), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        } catch (IngredientNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get Limit from ingredient from user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @return Success response entity that contains the limit for ingredient from user stock if user and ingredient exist
     * else return fail response entity
     */
    @GetMapping("/{userId}/ingredient/{ingredientId}/limit-quantity")
    public ResponseEntity<?> getIngredientLimit(@PathVariable Long userId, @PathVariable Long ingredientId) {
        try{
            return new ResponseEntity<>(IngredientToMap.getLimitToMap(
                    this.ingredientService.findById(ingredientId),
                    this.stockService.getIngredientLimit(userId, ingredientId)
            ), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }  catch (IngredientNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Ingredient not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock does not have ingredient"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Recalculate stock quantity for user's stock after Ingredient was cooked
     * @param userId : Long user Id
     * @param recipeId : Long ingredient Id
     * @return Success response entity that contains the updated stock quantity for given user and ingredient if user and ingredient exist
     * else return fail response entity
     */
    @PatchMapping("/{userId}/recipe/{recipeId}/recalculate-stock")
    public ResponseEntity<?> recalculateStock(@PathVariable Long userId, @PathVariable Long recipeId) {
        try{
            return new ResponseEntity<>(RecipeToMap.recalculateToMap(
                    this.stockService.recalculate(userId, recipeId),
                    this.stockService.getIngredientsUnderLimit(userId)),
                    HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }  catch (RecipeNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("Recipe not found"), HttpStatus.BAD_REQUEST);
        } catch (AopInvocationException aop){
            return new ResponseEntity<>(new ResponseFail("Stock lacks of ingredient"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all available recipes for an user
     * @param userId :Long user Id
     * @return ResponseEntity success that contains all available recipes for given user if user exists
     * else return fail response entity
     */
    @GetMapping("/{userId}/available-recipe")
    public ResponseEntity<?> getAvailableRecipes(@PathVariable Long userId) {
        try{
            return new ResponseEntity<>(RecipeToMap.availableRecipesToMap( this.stockService.getAllAvailableRecipes(userId)), HttpStatus.OK);
        } catch (UsernameNotFoundException e){
            return new ResponseEntity<>(new ResponseFail("User not found"), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Create a new stock in database
     * @param stock : StockDto stock
     * @return A new stock Entity if user from stock exists else error
     */
    @PostMapping
    @ResponseStatus(CREATED)
    public StockEntity create(@RequestBody @Valid StockDto stock){
        Optional<UserEntity> user = this.userService.findUserById(stock.getUser());
        if(user.isPresent()){
            return stockService.create(new StockEntity(stock.getName(), user.get()));
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }

    /**
     * Delete an ingredient from a user's stock
     * @param userId User's id to remove the ingredient from
     * @param ingredient_name Name of the ingredient to remove
     * @return Response entity to confirm deletion from user's stock, else return a failure message
     */
    @DeleteMapping("/{userId}/ingredient/{ingredient_name}")
    public ResponseEntity<?> deleteIngredientFromStock(@PathVariable Long userId, @PathVariable String ingredient_name) {
        try {
            this.stockService.deleteIngredientFromStock(userId, ingredient_name);
            return new ResponseEntity<>(new ResponseSuccess(ingredient_name + " has been removed from user " + userId, 0), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseFail(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}

package com.group6.projectjee.infrastructure.persistence.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RecipeMealDB {
    public List<Meal> meals;
}

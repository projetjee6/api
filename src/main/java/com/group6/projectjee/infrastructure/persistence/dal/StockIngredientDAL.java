package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface StockIngredientDAL extends CrudRepository<StockIngredient, Long> {
    boolean existsByStockEntity(StockEntity stockEntity);
    StockIngredient getByStockEntity(StockEntity stockEntity);
    boolean existsByStockEntityAndIngredientEntity(StockEntity stockEntity, IngredientEntity ingredientEntity);
    Optional<StockIngredient> getByIngredientEntityAndStockEntity(IngredientEntity ingredientEntity, StockEntity stockEntity);

    @Query(value = "Select quantity from StockIngredient where stock_id=:stockEntity and ingredient_id=:ingredientEntity")
    Float getQuantity(@Param("ingredientEntity") String ingredient, @Param("stockEntity") String stock);

    @Query(value = "Select minQuantity from StockIngredient where stock_id=:stockEntity and ingredient_id=:ingredientEntity")
    float getMinLimit(@Param("ingredientEntity") String ingredient, @Param("stockEntity") String stock);

    @Query(value = "Select ingredientEntity from StockIngredient where stock_id=:stockEntity")
    List<IngredientEntity> getAllIngredientEntityId(@Param("stockEntity") String stock);

    @Modifying
    @Transactional
    @Query(value = "UPDATE StockIngredient set quantity=:newQuantity where stock_id=:stockEntity and ingredient_id=:ingredientEntity")
    void updateQuantity(@Param("ingredientEntity") String ingredient, @Param("stockEntity") String stock, @Param("newQuantity") Float newQuantity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE StockIngredient set minQuantity=:newLimit where stock_id=:stockEntity and ingredient_id=:ingredientEntity")
    void updateLimit(@Param("ingredientEntity") String ingredient, @Param("stockEntity") String stock, @Param("newLimit") Float newLimit);
}

package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeDAL extends CrudRepository<RecipeEntity, Long> {
    Boolean existsByName(String name);
    RecipeEntity findByName(String name);
}

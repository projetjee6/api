package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientDAL extends CrudRepository<IngredientEntity, Long> {
    Boolean existsByName(String name);

    @Query("SELECT i from IngredientEntity i where i.name=:name")
    IngredientEntity findByName(String name);
    Iterable<IngredientEntity> findAll();
}

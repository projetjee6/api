package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RecipeIngredientDAL extends CrudRepository<RecipeIngredient, Long> {
    @EntityGraph(value = "recipeIngredient-entity-graph", type = EntityGraph.EntityGraphType.FETCH)
    ArrayList<RecipeIngredient> findAllByRecipeEntity(RecipeEntity recipeEntity);
    ArrayList<RecipeIngredient> findAllByIngredientEntity(IngredientEntity ingredientEntity);
    boolean existsByRecipeEntityNameAndIngredientEntityName(String recipe_name, String ingredient_name);
    ArrayList<RecipeIngredient> getAllByIngredientEntity(IngredientEntity ingredientEntity);
    RecipeIngredient getByRecipeEntityAndIngredientEntity(RecipeEntity recipeEntity, IngredientEntity ingredientEntity);
}

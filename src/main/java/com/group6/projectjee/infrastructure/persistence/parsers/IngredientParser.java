package com.group6.projectjee.infrastructure.persistence.parsers;

import com.group6.projectjee.domain.models.Ingredient;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;

public class IngredientParser {

    public static IngredientEntity parse(Ingredient ingredient){
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setId(ingredient.getId());
        ingredientEntity.setName(ingredient.getName());
        ingredientEntity.setBaseUnit(ingredient.getBaseUnit());
        ingredientEntity.setRecipeIngredients(ingredient.getRecipeIngredients());
        ingredientEntity.setStockIngredients(ingredient.getStockIngredients());
        return ingredientEntity;
    }

    public static Ingredient parse(IngredientEntity ingredientEntity){
        Ingredient ingredient = new Ingredient();
        ingredient.setId(ingredientEntity.getId());
        ingredient.setName(ingredientEntity.getName());
        ingredient.setBaseUnit(ingredientEntity.getBaseUnit());
        ingredient.setRecipeIngredients(ingredientEntity.getRecipeIngredients());
        ingredient.setStockIngredients(ingredientEntity.getStockIngredients());
        return ingredient;
    }
}

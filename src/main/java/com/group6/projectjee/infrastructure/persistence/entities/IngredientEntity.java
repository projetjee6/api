package com.group6.projectjee.infrastructure.persistence.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.serializer.RecipeIngredientSerializer;
import com.group6.projectjee.infrastructure.persistence.entities.serializer.StockIngredientsSerializer;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@Data
@Entity
public class IngredientEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private BaseUnit baseUnit;

    @OneToMany(mappedBy = "ingredientEntity", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<RecipeIngredient> recipeIngredients;

    @OneToMany(mappedBy = "ingredientEntity", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<StockIngredient> stockIngredients;

    public IngredientEntity(String name, BaseUnit baseUnit) {
        this.name = name;
        this.baseUnit = baseUnit;
    }

    public Long getId() {
        return this.id;
    }

    @JsonSerialize(using = RecipeIngredientSerializer.class)
    public List<RecipeIngredient> getRecipeIngredients(){
        return this.recipeIngredients;
    }

    @JsonSerialize(using = StockIngredientsSerializer.class)
    public List<StockIngredient> getStockIngredients(){
        return this.stockIngredients;
    }

    @Override
    public String toString() {
        return "IngredientEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", baseUnit=" + baseUnit +
                '}';
    }
}
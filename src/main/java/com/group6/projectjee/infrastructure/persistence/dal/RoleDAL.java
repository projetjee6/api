package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.domain.models.enums.ERole;
import com.group6.projectjee.infrastructure.persistence.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleDAL extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}

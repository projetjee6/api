package com.group6.projectjee.infrastructure.persistence.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@NamedEntityGraph(
        name = "stockIngredient-entity-graph",
        attributeNodes = {
                @NamedAttributeNode(value = "ingredientEntity", subgraph = "ingredient-entity-graph")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "ingredient-entity-graph",
                        attributeNodes = {
                                @NamedAttributeNode("id"),
                                @NamedAttributeNode("name"),
                                @NamedAttributeNode("baseUnit"),
                        }
                )
        }
)
@Data
@Getter
@Entity
@NoArgsConstructor
@IdClass(StockIngredientID.class)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"stock_id", "ingredient_id"}))
public class StockIngredient implements Serializable {
    // jointure stock - ingredient

    @Id
    @ManyToOne
    @JoinColumn(name = "stock_id", referencedColumnName = "id")
    @JsonBackReference
    private StockEntity stockEntity;

    @Id
    @ManyToOne
    @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    @JsonBackReference
    private IngredientEntity ingredientEntity;

    @Column(nullable = false)
    private float quantity;

    @Column(nullable = false)
    private float minQuantity;

    public StockIngredient(StockEntity stockEntity, IngredientEntity ingredientEntity, float quantity, float minQuantity) {
        this.stockEntity = stockEntity;
        this.ingredientEntity = ingredientEntity;
        this.quantity = quantity;
        this.minQuantity = minQuantity;
    }

}

package com.group6.projectjee.infrastructure.persistence.dal;

import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockDAL extends CrudRepository<StockEntity, Long> {
    StockEntity findByUserEntity(UserEntity userEntity);
}

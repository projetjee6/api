package com.group6.projectjee.infrastructure.persistence.entities;

import java.io.Serializable;

public class RecipeIngredientID implements Serializable {

    private Long recipeEntity;
    private Long ingredientEntity;

    @Override
    public int hashCode() {
        Long hash = 1L;
        hash = hash * 17 + recipeEntity;
        hash = hash * 31 + ingredientEntity;
        return Integer.parseInt(hash.toString());
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}

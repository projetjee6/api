package com.group6.projectjee.infrastructure.persistence.parsers;

import com.group6.projectjee.domain.models.Recipe;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;

public class RecipeParser {

    public static RecipeEntity parse(Recipe recipe){
        RecipeEntity recipeEntity = new RecipeEntity();
        recipeEntity.setId(recipe.getId());
        recipeEntity.setName(recipe.getName());
        recipeEntity.setCreatedAt(recipe.getCreatedAt());
        recipeEntity.setUpdatedAt(recipe.getUpdatedAt());
        recipeEntity.setRecipeIngredients(recipe.getRecipeIngredients());
        return recipeEntity;
    }

    public static Recipe parse(RecipeEntity recipeEntity){
        Recipe recipe = new Recipe();
        recipe.setId(recipeEntity.getId());
        recipe.setName(recipeEntity.getName());
        recipe.setCreatedAt(recipeEntity.getCreatedAt());
        recipe.setUpdatedAt(recipeEntity.getUpdatedAt());
        recipe.setRecipeIngredients(recipeEntity.getRecipeIngredients());
        return recipe;
    }
}

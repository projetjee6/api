package com.group6.projectjee.domain.models.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}

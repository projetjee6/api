package com.group6.projectjee.domain.models.enums;

public enum BaseUnit {
    G,
    CL,
    KG,
    ML,
    L,
    CUP,
    TEASPOON,
    TABLESPOON,
    NO_UNIT;

    public static boolean contains(String test) {
        for (BaseUnit baseUnit : BaseUnit.values()) {
            if (baseUnit.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}

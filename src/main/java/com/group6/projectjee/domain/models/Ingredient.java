package com.group6.projectjee.domain.models;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;
import io.swagger.models.auth.In;

import java.util.List;

public class Ingredient extends ModelBase{

    private String name;
    private BaseUnit baseUnit;
    private List<RecipeIngredient> recipeIngredients;
    private List<StockIngredient> stockIngredients;

    public Ingredient(){
        super();
    }

    public Ingredient(String name, BaseUnit baseUnit, List<RecipeIngredient> recipeIngredients, List<StockIngredient> stockIngredients) {
        this.name = name;
        this.baseUnit = baseUnit;
        this.recipeIngredients = recipeIngredients;
        this.stockIngredients = stockIngredients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseUnit getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(BaseUnit baseUnit) {
        this.baseUnit = baseUnit;
    }

    public List<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public List<StockIngredient> getStockIngredients() {
        return stockIngredients;
    }

    public void setStockIngredients(List<StockIngredient> stockIngredients) {
        this.stockIngredients = stockIngredients;
    }
}

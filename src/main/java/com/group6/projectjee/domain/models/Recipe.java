package com.group6.projectjee.domain.models;

import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;

import java.util.Date;
import java.util.List;

public class Recipe extends ModelBase {

    private String name;
    private List<RecipeIngredient> recipeIngredients;
    private Date createdAt;
    private Date updatedAt;

    public Recipe(){
        super();
    }

    public Recipe(String name, List<RecipeIngredient> recipeIngredients, Date createdAt, Date updatedAt) {
        this.name = name;
        this.recipeIngredients = recipeIngredients;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

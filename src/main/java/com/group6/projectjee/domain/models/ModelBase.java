package com.group6.projectjee.domain.models;

public class ModelBase {
    protected Long id;

    public ModelBase() {
    }

    public ModelBase(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

 package com.group6.projectjee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication()
@EnableJpaAuditing // Pour les données auto-générées des entités ex : createdAt et updatedAt
public class ProjectJeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectJeeApplication.class, args);
    }

}

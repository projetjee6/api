package com.group6.projectjee.use_case.services;

import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.use_case.exceptions.IngredientNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.logging.Logger;

@Service
public class IngredientService {

    private final IngredientDAL ingredientDAL;
    private final Logger logger;

    public IngredientService(IngredientDAL ingredientDAL) {
        this.ingredientDAL = ingredientDAL;
        this.logger = Logger.getLogger("IngredientService");
    }

    /**
     * Create a new ingredient into Database
     * @param ingredientEntity : IngredientEntity ingredient we want to add
     * @return created ingredientEntity
     * @throws DataIntegrityViolationException
     */
    public IngredientEntity create(IngredientEntity ingredientEntity) throws DataIntegrityViolationException{
        return this.ingredientDAL.save(ingredientEntity);
    }

    /**
     * Get all ingredients from database
     * @return Iterable that contains all ingredients from database
     */
    public Iterable<IngredientEntity> getAllIngredients(){
        return this.ingredientDAL.findAll();
    }

    /**
     * Finds if an ingredient exists given an ingredient name in database
     * @param name : String searched ingredient
     * @return true if database contains given ingredient else false
     */
    public Boolean existsByName(String name) {
        return this.ingredientDAL.existsByName(name);
    }

    /**
     * Finds if an ingredient exists given an ingredient id in database
     * @param id : Long searched ingredient
     * @return true if database contains given ingredient else false
     */
    public Boolean existsById(Long id) {
        if (this.ingredientDAL.existsById(id)) {
            return this.ingredientDAL.existsById(id);
        } else {
            throw new IngredientNotFoundException(id.toString());
        }
    }

    /**
     * Finds if an ingredient exists given an ingredient name in database
     * @param name : String searched ingredient
     * @return ingredient if ingredient exists else
     * @throws IngredientNotFoundException
     */
    public IngredientEntity findByName(String name) {
        IngredientEntity ingredientEntity = this.ingredientDAL.findByName(name);
        if(ingredientEntity == null) throw new IngredientNotFoundException(name);
        return ingredientEntity;
    }

    /**
     * Finds if an ingredient exists given an ingredient id in database
     * @param id : Long searched ingredient
     * @return ingredient if ingredient exists else
     * @throws IngredientNotFoundException
     */
    public IngredientEntity findById(Long id){
        Optional<IngredientEntity> ingredientEntity = this.ingredientDAL.findById(id);
        if(ingredientEntity.isPresent()) {
            return ingredientEntity.get();
        } else {
            throw new IngredientNotFoundException("");
        }

    }
}

package com.group6.projectjee.use_case.services;

import com.group6.projectjee.infrastructure.persistence.dal.StockIngredientDAL;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;
import org.springframework.aop.AopInvocationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StockIngredientService {

    private final StockIngredientDAL stockIngredientDAL;
    private final IngredientService ingredientService;

    public StockIngredientService(StockIngredientDAL stockIngredientDAL, IngredientService ingredientService) {
        this.stockIngredientDAL = stockIngredientDAL;
        this.ingredientService = ingredientService;
    }

    /**
     * Insert a new StockIngredient into database
     * @param stockIngredient : StockIngredient to insert
     * @return created StockIngredient
     */
    public StockIngredient create(StockIngredient stockIngredient) {
        return this.stockIngredientDAL.save(stockIngredient);
    }

    public void delete(StockIngredient stockIngredient) {
        this.stockIngredientDAL.delete(stockIngredient);
    }

    public Optional<StockIngredient> findByIngredientAndStock(IngredientEntity ingredientEntity, StockEntity stockEntity) {
        return this.stockIngredientDAL.getByIngredientEntityAndStockEntity(ingredientEntity, stockEntity);
    }

    /**
     * Get a join table given a stock
     * @param stockEntity : Stock
     * @return StockIngredient from given stock
     */
    public StockIngredient getByStock(StockEntity stockEntity){
        return stockIngredientDAL.getByStockEntity(stockEntity);
    }

    /**
     * Find if a StockIngredient exists given a stock
     * @param stockEntity : Stock
     * @return true if StockIngredient exists by given stock, else false
     */
    public Boolean existsByStock(StockEntity stockEntity){
        return this.stockIngredientDAL.existsByStockEntity(stockEntity);
    }

    /**
     * Add a given quantity to a given ingredient
     * @param ingredientEntity : Long ingredient id
     * @param quantity : Float quantity to add
     * @param stockEntity : Stock worked on
     */
    public void addIngredientQuantity(IngredientEntity ingredientEntity, float quantity, StockEntity stockEntity){
        if(!this.existsByStockAndIngredient(stockEntity, ingredientEntity)) {
            this.create(new StockIngredient(stockEntity, ingredientEntity, 0f, 0f));
        }
        this.stockIngredientDAL.updateQuantity(String.valueOf(ingredientEntity.getId()), String.valueOf(stockEntity.getId()), quantity + this.getQuantity(ingredientEntity, stockEntity));
    }

    /**
     * Get an ingredient quantity given an ingredient and a stock
     * @param ingredientEntity : ingredient
     * @param stockEntity : stock
     * @return Float ingredient's quantity in the stock
     */
    public float getQuantity(IngredientEntity ingredientEntity, StockEntity stockEntity) {
        Float result = this.stockIngredientDAL.getQuantity(ingredientEntity.getId().toString(), stockEntity.getId().toString());
        if(result == null) {
            return 0f;
        }
        return result;
    }

    /**
     * Update an ingredient's quantity in a given stock
     * @param ingredientEntity : ingredient
     * @param stockEntity : stock
     * @param quantity : Float new ingredient's quantity
     */
    public void updateQuantity(IngredientEntity ingredientEntity, StockEntity stockEntity, float quantity) {
        if(!this.existsByStockAndIngredient(stockEntity, ingredientEntity)) {
            this.create(new StockIngredient(stockEntity, ingredientEntity, 0f, 0f));
        }
        this.stockIngredientDAL.updateQuantity(String.valueOf(ingredientEntity.getId()), String.valueOf(stockEntity.getId()), quantity);
    }

    /**
     * Update an ingredient's limit in a given stock
     * @param ingredientEntity : ingredient
     * @param stockEntity : stock
     * @param quantity : Float new ingredient's limit
     */
    public void updateLimit(IngredientEntity ingredientEntity, StockEntity stockEntity, float quantity) {
        this.stockIngredientDAL.updateLimit(String.valueOf(ingredientEntity.getId()), String.valueOf(stockEntity.getId()), quantity);
    }

    /**
     * Get limit for a given ingredient in a given stock
     * @param ingredientEntity : ingredient
     * @param stockEntity : stock
     * @return Float ingredient's limit in stock
     * @throws AopInvocationException
     */
    public float getLimit(IngredientEntity ingredientEntity, StockEntity stockEntity) throws AopInvocationException{
        return this.stockIngredientDAL.getMinLimit(String.valueOf(ingredientEntity.getId()), String.valueOf(stockEntity.getId()));
    }

    /**
     * Get all Ingredients from a given stock
     * @param stockEntity : stock
     * @return ArrayList that contains all ingredients in the stock
     */
    public ArrayList<IngredientEntity> getAllIngredientFromStock(StockEntity stockEntity){
        List<IngredientEntity> id = this.stockIngredientDAL.getAllIngredientEntityId(String.valueOf(stockEntity.getId()));
        ArrayList<IngredientEntity> res = new ArrayList<>();
        for (IngredientEntity ingredientEntity : id){
            res.add(ingredientEntity);
        }
        return res;
    }

    public Boolean existsByStockAndIngredient(StockEntity stock, IngredientEntity ingredient) {
        return this.stockIngredientDAL.existsByStockEntityAndIngredientEntity(stock, ingredient);
    }
}

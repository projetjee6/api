package com.group6.projectjee.use_case.services.authent;

import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserDAL userDAL;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userDAL.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return UserDetailsImpl.build(userEntity);
    }
}

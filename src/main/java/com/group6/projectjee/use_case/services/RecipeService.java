package com.group6.projectjee.use_case.services;

import com.google.api.gax.rpc.AlreadyExistsException;
import com.google.gson.Gson;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeDAL;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import com.group6.projectjee.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.use_case.exceptions.RecipeNotFoundException;
import com.group6.projectjee.web.controllers.post.IngredientList;
import com.group6.projectjee.web.controllers.post.RecipeDto;
import com.group6.projectjee.web.controllers.post.RecipeIngredientDto;
import com.group6.projectjee.web.controllers.responses.ResponseFail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;

@Service
public class RecipeService {

    private final RecipeDAL recipeDAL;
    private final RecipeIngredientService recipeIngredientService;
    private final RestService restService;
    private final Logger logger;
    private final Gson g;
    private final IngredientService ingredientService;

    @Autowired
    public RecipeService(RecipeDAL recipeDAL, RecipeIngredientService recipeIngredientService, RestService restService, IngredientService ingredientService){
        this.recipeDAL = recipeDAL;
        this.recipeIngredientService = recipeIngredientService;
        this.restService = restService;
        this.ingredientService = ingredientService;
        this.g = new Gson();
        this.logger = Logger.getLogger("RecipeService");

    }

    public RecipeEntity create(RecipeEntity recipeEntity) throws Exception {
        if (existsByName(recipeEntity.getName())){
            throw new Exception("Recipe already exist");
        }
        return this.recipeDAL.save(recipeEntity);
    }

    /**
     * Get all recipe from database
     * @return Iterable that contains all data from database
     */
    public Iterable<RecipeEntity> getAllRecipe(){
        return this.recipeDAL.findAll();
    }

    /**
     * Find recipe into database by given name
     * @param name : String ingredient name
     * @return Recipe if ingredient exists else
     * @throws RecipeNotFoundException
     */
    public RecipeEntity findRecipeByName(String name){
        if(this.recipeDAL.existsByName(name))
            return this.recipeDAL.findByName(name);
        else {
            throw new RecipeNotFoundException(name);
        }
    }

    /**
     * Find recipe into database by given od
     * @param id : Long ingredient id
     * @return Recipe if ingredient exists else
     * @throws RecipeNotFoundException
     */
    public RecipeEntity findRecipeById(Long id) {
        if (existsById(id)){
            return this.recipeDAL.findById(id).get();
        } else {
            throw new RecipeNotFoundException(id.toString());
        }
    }

    /**
     * Find recipe into database by given od
     * @param id : Long ingredient id
     * @return true if ingredient exists else
     * @throws RecipeNotFoundException
     */
    public Boolean existsById(Long id) {
        if( this.recipeDAL.existsById(id)){
            return this.recipeDAL.existsById(id);
        } else {
            throw new RecipeNotFoundException(id.toString());
        }
    }
    public Boolean existsByName(String name) {
        if( this.recipeDAL.existsByName(name)){
            return this.recipeDAL.existsByName(name);
        }
        return false;
    }

    /**
     * Get a random recipe from database
     * @return Random recipe from database
     */
    public RecipeEntity getRandomRecipe(){
        ArrayList<RecipeEntity> allRecipeEntities = (ArrayList<RecipeEntity>) getAllRecipe();
        Random rand = new Random();
        return allRecipeEntities.get(rand.nextInt(allRecipeEntities.size()));
    }

    /**
     * Get all ingredients in a given recipe
     * @param recipe_id : parsed Recipe
     * @return ArrayList of all ingredients
     */
    public ArrayList<IngredientEntity> getRecipeIngredients(Long recipe_id){
        if(!this.recipeDAL.existsById(recipe_id)) {
            throw new RecipeNotFoundException(recipe_id.toString());
        }
        return this.recipeIngredientService.getRecipeIngredients(this.findRecipeById(recipe_id));
    }

    /**
     * Find if a recipe contains given ingredient
     * @param recipe_name : String recipe name
     * @param ingredient_name : String ingredient name
     * @return True if recipe contains ingredients, else false
     * @throws RecipeNotFoundException if recipe is not found
     * @throws IngredientNotFoundException if ingredient is not found
     */
    public boolean hasIngredient(String recipe_name, String ingredient_name) {
        if(!this.recipeDAL.existsByName(recipe_name)) {
            throw new RecipeNotFoundException(recipe_name);
        }
        if(!this.ingredientService.existsByName(ingredient_name)) {
            throw new IngredientNotFoundException(ingredient_name);
        }
        return this.recipeIngredientService.existsByName(recipe_name, ingredient_name);
    }

    public void addIngredientToRecipe(RecipeIngredientDto recipeIngredientDto) {
        if(recipeIngredientDto.getQuantity() <= 0) {
            throw new RuntimeException("Quantity must be greater than 0");
        }
        if(!this.recipeDAL.existsByName(recipeIngredientDto.getRecipe_name())) {
            throw new RecipeNotFoundException(recipeIngredientDto.getRecipe_name());
        }
        if(!this.ingredientService.existsByName(recipeIngredientDto.getIngredient_name())) {
            throw new IngredientNotFoundException(recipeIngredientDto.getIngredient_name());
        }

        if(this.recipeIngredientService.existsByName(recipeIngredientDto.getRecipe_name(), recipeIngredientDto.getIngredient_name())) {
            throw new RuntimeException(recipeIngredientDto.getIngredient_name() + " is already used by " + recipeIngredientDto.getRecipe_name());
        }

        this.recipeIngredientService.create(new RecipeIngredient(
                this.findRecipeByName(recipeIngredientDto.getRecipe_name()),
                ingredientService.findByName(recipeIngredientDto.getIngredient_name()),
                recipeIngredientDto.getQuantity()
        ));
    }

    /**
     * Get the total price of a given recipe
     * @param recipe_id : parsed recipe
     * @return String price of given recipe
     * @throws RecipeNotFoundException if recipe does not exist
     */
    public String getIngredientsPrice(Long recipe_id) {
        if(!existsById(recipe_id)) {
            throw new RecipeNotFoundException(recipe_id.toString());
        }
        RecipeEntity recipeEntity = findRecipeById(recipe_id);
        ArrayList<IngredientEntity> ingredientEntities = recipeIngredientService.getRecipeIngredients(recipeEntity);
        Float sum = 0.0f;
        for (IngredientEntity ingredientEntity :
                ingredientEntities) {
            if(restService.getIngredientPrice(ingredientEntity).length == 0) {
                logger.warning("Couldn't get ingredient price for " + ingredientEntity.getName() + ", FoodStore API is probably down");
                return "Couldn't get ingredient price for " + ingredientEntity.getName() + ", FoodStore API is probably down";
            }
            sum += restService.getIngredientPrice(ingredientEntity)[0].getPrice();
        }
        Map<String, String> result = new HashMap<String, String>();
        result.put("recipeName", recipeEntity.getName());
        result.put("estimatedPrice", sum.toString());
        return g.toJson(result);
    }

    public ArrayList<RecipeDto> getRecipeByIngredients(IngredientList ingredientList) {
        ArrayList<RecipeDto> result = new ArrayList<RecipeDto>();
        for (String ingredient :
                ingredientList.getIngredients()) {
            if(!this.ingredientService.existsByName(ingredient)) {
                throw new IngredientNotFoundException(ingredient);
            }
            ArrayList<RecipeEntity> recipesUsingIngredient = this.recipeIngredientService.getRecipesByIngredient(
                    this.ingredientService.findByName(ingredient),
                    this
            );
            for (RecipeEntity recipe :
                    recipesUsingIngredient) {
                if (!result.contains(recipe.toDto()) &&
                    this.recipeIngredientService.useAllIngredients(recipe, ingredientList.getIngredients())) {
                    result.add(recipe.toDto());
                }
            }
        }
        return result;
    }

}

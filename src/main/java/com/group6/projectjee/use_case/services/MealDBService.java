package com.group6.projectjee.use_case.services;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.Meal;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class MealDBService {

    private final IngredientService ingredientService;
    private final RecipeIngredientService recipeIngredientService;
    private final RecipeService recipeService;
    private final Logger logger;

    @Autowired
    public MealDBService(IngredientService ingredientService, RecipeIngredientService recipeIngredientService, RecipeService recipeService) {
        this.ingredientService = ingredientService;
        this.recipeIngredientService = recipeIngredientService;
        this.recipeService = recipeService;
        this.logger = Logger.getLogger("MealDBService");
    }

    /**
     * Add ingredients to database for each ingredients contained in external recipe
     * @param meal : Meal being parsed
     */
    public void checkIngredients(Meal meal) {
        HashMap<String, String> mealIngredients = meal.getIngredientsMap();

        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            if(!ingredientService.existsByName(entry.getKey())) {
                BaseUnit unit = this.parseUnit(entry.getValue());
                ingredientService.create(new IngredientEntity((String) entry.getKey(), unit));
            }
        }
    }

    /**
     * Transform a measure from external recipe into usable Enum BaseUnit
     * @param measure : String parsed
     * @return BaseUnit for given ingredient
     */
    public BaseUnit parseUnit(String measure) {
        if(measure.matches("[0-9]+ ?g(rammes?)?")) {
            return BaseUnit.G;
        } else if(measure.matches("[0-9]+ ?(cl|centilitres?)")) {
            return BaseUnit.CL;
        } else if(measure.matches("[0-9]+ ?(kg|kilogrammes?)")) {
            return BaseUnit.KG;
        } else if(measure.matches("[0-9]+ ?(ml|millilitres?)")) {
            return BaseUnit.ML;
        } else if(measure.matches("[0-9]+ ?[lL](itres?)?")) {
            return BaseUnit.L;
        } else if(measure.matches("([uU]ne? )?([cC]oupe|[Tt]asse|[Gg]obelet)")) {
            return BaseUnit.CUP;
        } else if(measure.matches("(([1-9](/[1-9])? ?)|[Uu]ne )?[cC]uillères? à café.*")) {
            return BaseUnit.TEASPOON;
        } else if(measure.matches("(([1-9](/[1-9])? ?)|[Uu]ne )?[cC]uillères? à soupe.*")) {
            return BaseUnit.TABLESPOON;
        }
        return BaseUnit.NO_UNIT;
    }

    /**
     * Parse a meal from external api into usable recipe
     * @param meal : Meal parsed
     * @return new recipe from parsed data
     */
    public RecipeEntity toRecipe(Meal meal) throws Exception {
        RecipeEntity recipeEntity = recipeService.create(new RecipeEntity(meal.getStrMeal()));
        HashMap<String, String> mealIngredients = meal.getIngredientsMap();
        for (Map.Entry<String, String> entry : mealIngredients.entrySet()) {
            recipeIngredientService.create(new RecipeIngredient(recipeEntity, ingredientService.findByName(entry.getKey()), this.parseQuantity(entry.getValue())));
        }
        return recipeEntity;
    }

    /**
     * Get the quantity from external meal
     * @param quantity : String parsed quantity
     * @return Float quantity
     */
    public Float parseQuantity(String quantity) {
        if(quantity.matches("([1-9]/[2-9])|½")) {
            String[] calc = quantity.split("/");
            if(Float.parseFloat(calc[1]) == 0){
                return Float.parseFloat(calc[0]);
            }
            return Float.parseFloat(calc[0])/Float.parseFloat(calc[1]);
        }
        quantity = quantity.split(" ")[0];
        if(quantity.replaceAll("[^0-9]", "").equals("")) {
            return 1.0f;
        }

        return Float.parseFloat(quantity.replaceAll("[^0-9]", ""));
    }

}

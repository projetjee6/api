package com.group6.projectjee.use_case.services;

import com.group6.projectjee.infrastructure.persistence.dal.StockDAL;
import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import com.group6.projectjee.infrastructure.persistence.entities.*;
import com.group6.projectjee.use_case.exceptions.IngredientNotFoundException;
import com.group6.projectjee.use_case.exceptions.UserNotFoundException;
import com.group6.projectjee.use_case.exceptions.UsernameNotFoundException;
import org.springframework.aop.AopInvocationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StockService {

    private final UserDAL userDAL;
    private final StockDAL stockDAL;
    private final StockIngredientService stockIngredientService;
    private final IngredientService ingredientService;
    private final RecipeService recipeService;
    private final RecipeIngredientService recipeIngredientService;

    public StockService(UserDAL userDAL, StockDAL stockDAL, StockIngredientService stockIngredientService, IngredientService ingredientService, RecipeService recipeService, RecipeIngredientService recipeIngredientService) {
        this.userDAL = userDAL;
        this.stockDAL = stockDAL;
        this.stockIngredientService = stockIngredientService;
        this.ingredientService = ingredientService;
        this.recipeService = recipeService;
        this.recipeIngredientService = recipeIngredientService;
    }

    /**
     * Return all ingredients that given user has
     * @param userId : Long
     * @return ArrayList that contains all ingredients from user
     */
    public ArrayList<IngredientEntity> getUserIngredients(long userId){
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            if(stockEntity == null){
                this.stockDAL.save(new StockEntity("", user.get()));
            }
            return this.stockIngredientService.getAllIngredientFromStock(stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Get the quantity of a given ingredient from a given user's stock
     * @param userId : Long user Id
     * @param ingredientId : Long Ingredient Id
     * @return Float ingredient's quantity
     * @throws UsernameNotFoundException if user not found
     */
    public float getUserIngredientQuantity(long userId, Long ingredientId) {
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            if(stockEntity == null){
                this.stockDAL.save(new StockEntity("", user.get()));
            }
            IngredientEntity ingredientEntity = this.ingredientService.findById(ingredientId);
            return this.stockIngredientService.getQuantity(ingredientEntity, stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Internal use only for calculating purposes
     * @param userId : Long user Id
     * @param ingredientEntity : Long ingredient Id
     * @return Float quantity if user
     */
    private float getUserIngredientQuantityInternal(long userId, IngredientEntity ingredientEntity){
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent() && !user.get().getStockEntityList().isEmpty()){
            StockEntity stockEntity = user.get().getStockEntityList().get(0);
            try {
                return getIngredientQuantityFromStockEntity(stockEntity, ingredientEntity.getName());
            } catch (IngredientNotFoundException e){
                return -2;
            }
        } else {
            return -1;
        }
    }

    public float getIngredientQuantityFromStockEntity(StockEntity stockEntity, String ingredient_name) {
        for (StockIngredient stockIngredient :
                stockEntity.getStockIngredients()) {
            if (stockIngredient.getIngredientEntity().getName() == ingredient_name) {
                return stockIngredient.getQuantity();
            }
        }
        return 0.f;
    }

    public float addIngredientToStock(long userId, Long ingredientId, float quantity){
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
                StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
                if(stockEntity == null){
                    this.stockDAL.save(new StockEntity("", user.get()));
                }
                IngredientEntity ingredientEntity = this.ingredientService.findById(ingredientId);
                this.stockIngredientService.create(new StockIngredient(stockEntity, ingredientEntity, quantity, 0));
                return this.stockIngredientService.getQuantity(ingredientEntity, stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Add quantity to a given ingredient
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient id
     * @param quantity : float quantity to add
     * @return updated ingredient's quantity
     * @throws UsernameNotFoundException
     */
    public float addIngredientQuantity(long userId, Long ingredientId, float quantity) {
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            if(stockEntity == null) {
                this.stockDAL.save(new StockEntity("", user.get()));
            }
                IngredientEntity ingredientEntity = this.ingredientService.findById(ingredientId);
                this.stockIngredientService.addIngredientQuantity(ingredientEntity, quantity, stockEntity);
                return this.stockIngredientService.getQuantity(ingredientEntity, stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Update quantity to a given ingredient
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient id
     * @param quantity : float quantity new quantity
     * @return updated ingredient's quantity
     * @throws UsernameNotFoundException
     */
    public float updateIngredientQuantity(long userId, Long ingredientId, float quantity) {
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            if(stockEntity == null){
                this.stockDAL.save(new StockEntity("", user.get()));
            }
                IngredientEntity ingredientEntity = this.ingredientService.findById(ingredientId);
                this.stockIngredientService.updateQuantity(ingredientEntity, stockEntity, quantity);
                return this.stockIngredientService.getQuantity(ingredientEntity, stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Updata limit to a given ingredient
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient id
     * @param quantity : float limit new limit
     * @return updated ingredient's quantity
     * @throws UsernameNotFoundException
     */
    public float updateIngredientLimit(long userId, Long ingredientId, float quantity) {
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            if(stockEntity == null){
                this.stockDAL.save(new StockEntity("", user.get()));
            }
                IngredientEntity ingredientEntity = this.ingredientService.findById(ingredientId);
                this.stockIngredientService.updateLimit(ingredientEntity, stockEntity, quantity);
                return this.stockIngredientService.getLimit(ingredientEntity, stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Get ingredient's limit
     * @param userId : Long user Id
     * @param ingredientId : Long ingredient Id
     * @return Float ingredient's limit
     * @throws UsernameNotFoundException
     */
    public float getIngredientLimit(long userId, Long ingredientId) {
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
                StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
                if(stockEntity == null){
                    this.stockDAL.save(new StockEntity("", user.get()));
                }
                IngredientEntity ingredientEntity = this.ingredientService.findById(ingredientId);
                this.stockIngredientService.getLimit(ingredientEntity, stockEntity);
                return this.stockIngredientService.getLimit(ingredientEntity, stockEntity);
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Recalculate user's stock after cooking a recipe
     * @param userId : Long user id
     * @param recipeId : Long recipe id
     * @return ArrayList of UsedIngredients
     * @throws UsernameNotFoundException
     */
    public ArrayList<IngredientEntity> recalculate(Long userId, Long recipeId) {
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
                RecipeEntity recipeEntity = this.recipeService.findRecipeById(recipeId);
                ArrayList<IngredientEntity> usedIngredientEntity = new ArrayList<>();
                ArrayList<IngredientEntity> ingredientEntities = recipeIngredientService.getRecipeIngredients(recipeEntity);
                for (IngredientEntity ingredientEntity : ingredientEntities) {
                    usedIngredientEntity.add(ingredientEntity);
                    this.updateIngredientQuantity(userId,
                            ingredientEntity.getId(),
                            this.getUserIngredientQuantityInternal(userId, ingredientEntity)
                                    - recipeIngredientService.getIngredientQuantity(recipeEntity, ingredientEntity));
                }
                return usedIngredientEntity;
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    public boolean availableRecipeInternal(Long userId, RecipeEntity recipeEntity){
        ArrayList<IngredientEntity> ingredientEntities = new ArrayList<>();
        if(recipeEntity.getRecipeIngredients() == null) {
            return false;
        }
        for (RecipeIngredient recipeIngredient :
                recipeEntity.getRecipeIngredients()) {
            ingredientEntities.add(recipeIngredient.getIngredientEntity());
        }
        for(IngredientEntity ingredientEntity : ingredientEntities){
            float neededQuantity = getIngredientQuantityFromRecipeEntity(recipeEntity, ingredientEntity.getName());
            float availableQuantity = this.getUserIngredientQuantityInternal(userId, ingredientEntity);
            if(availableQuantity < 0) return false;
            if(neededQuantity > availableQuantity) return false;
        }
        return true;
    }

    public float getIngredientQuantityFromRecipeEntity(RecipeEntity recipeEntity, String ingredient_name) {
        for (RecipeIngredient recipeIngredient :
                recipeEntity.getRecipeIngredients()) {
            if (recipeIngredient.getIngredientEntity().getName() == ingredient_name) {
                return recipeIngredient.getQuantity();
            }
        }
        return 0.f;
    }

    /**
     * Get all available recipes for a given user
     * @param userId : Long user id
     * @return ArrayList that contains all available recipes for user
     * @throws UsernameNotFoundException
     */
    public ArrayList<RecipeEntity> getAllAvailableRecipes(Long userId) {
        ArrayList<RecipeEntity> res = new ArrayList<>();
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()) {
            Iterable<RecipeEntity> allRecipes = this.recipeService.getAllRecipe();
            for(RecipeEntity recipeEntity : allRecipes){
                try {
                    if (this.availableRecipeInternal(userId, recipeEntity)) res.add(recipeEntity);
                } catch (AopInvocationException aop){
                    //do Nothing SKIP recipe
                }
            }
            return res;
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    /**
     * Get all user's ingredients under choosen limit
     * @param userId : Long user Id
     * @return ArrayList that contains all ingredients' quantity that is under limit
     */
    public ArrayList<IngredientEntity> getIngredientsUnderLimit(Long userId){
        Optional<UserEntity> user = this.userDAL.findById(userId);
        if(user.isPresent()){
            ArrayList<IngredientEntity> res = new ArrayList<>();
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            if(stockEntity == null){
                this.stockDAL.save(new StockEntity("", user.get()));
            }
            for (IngredientEntity ingredientEntity:this.stockIngredientService.getAllIngredientFromStock(stockEntity)) {
                 if(this.stockIngredientService.getLimit(ingredientEntity, stockEntity) >
                        this.stockIngredientService.getQuantity(ingredientEntity, stockEntity)){
                     res.add(ingredientEntity);
                 }
            }
            return res;
        } else {
            throw new UsernameNotFoundException("");
        }
    }

    /**
     * Insert a new Stock into database
     * @param stockEntity : Stock to insert
     * @return Inserted stock
     */
    public StockEntity create(StockEntity stockEntity) {
            return this.stockDAL.save(stockEntity);
    }

    public void deleteIngredientFromStock(Long userId, String ingredient_name) {
        Optional<UserEntity> user = this.userDAL.findById(userId);

        if (!this.ingredientService.existsByName(ingredient_name)) {
            throw new IngredientNotFoundException(ingredient_name);
        }

        if (user.isPresent()){
            StockEntity stockEntity = this.stockDAL.findByUserEntity(user.get());
            Optional<StockIngredient> stockIngredient = this.stockIngredientService.findByIngredientAndStock(this.ingredientService.findByName(ingredient_name), stockEntity);
            if(stockIngredient.isPresent()) {
                this.stockIngredientService.delete(stockIngredient.get());
            } else {
                throw new RuntimeException(ingredient_name + " isn't in " + user.get().getUsername() + "'s stock");
            }
        } else {
            throw new UsernameNotFoundException("");
        }
    }
}

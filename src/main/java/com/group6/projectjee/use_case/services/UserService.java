package com.group6.projectjee.use_case.services;


import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserDAL userDAL;

    @Autowired
    public UserService(UserDAL userDAL){
        this.userDAL = userDAL;
    }

    /**
     * Insert a new user into database
     * @param userEntity : user we're inserting into database
     * @return Inserted user
     */
    public UserEntity create(UserEntity userEntity){
        return this.userDAL.save(userEntity);
    }

    /**
     * Delete a given user from database
     * @param userEntity : user to delete
     */
    public void delete(UserEntity userEntity){
        this.userDAL.delete(userEntity);
    }

    /**
     * Get all user from database
     * @return Iterable that contains all users from database
     */
    public Iterable<UserEntity> getAllUsers()
    {
        return this.userDAL.findAll();
    }

    /**
     * Find user given an id
     * @param userId : Long user Id
     * @return User if user exists else null
     */
    public Optional<UserEntity> findUserById(long userId){
        if(this.userDAL.existsById(userId))
            return this.userDAL.findById(userId);
        else return null;
    }

}

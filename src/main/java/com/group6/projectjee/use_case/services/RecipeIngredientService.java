package com.group6.projectjee.use_case.services;

import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeIngredientDAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.logging.Logger;

@Service
public class RecipeIngredientService {

    private final RecipeIngredientDAL recipeIngredientDAL;
    private final IngredientService ingredientService;
    private final Logger logger;

    @Autowired
    public RecipeIngredientService(RecipeIngredientDAL recipeIngredientDAL, IngredientService ingredientService) {
        this.recipeIngredientDAL = recipeIngredientDAL;
        this.ingredientService = ingredientService;
        this.logger = Logger.getLogger("RecipeIngredientService");
    }

    public RecipeIngredient create(RecipeIngredient recipeIngredient) { return this.recipeIngredientDAL.save(recipeIngredient); }

    /**
     * Get all ingredients from a given recipe
     * @param recipeEntity : Recipe
     * @return ArrayList of ingredients
     */
    public ArrayList<IngredientEntity> getRecipeIngredients(RecipeEntity recipeEntity) {
        ArrayList<RecipeIngredient> results = recipeIngredientDAL.findAllByRecipeEntity(recipeEntity);
        ArrayList<IngredientEntity> ingredientEntities = new ArrayList<IngredientEntity>();
        for (RecipeIngredient entry : results) {
            ingredientEntities.add(entry.getIngredientEntity());
        }
        return ingredientEntities;
    }

    /**
     * Get ingredient's quantity in a given recipe
     * @param recipeEntity : parsed recipe
     * @param ingredientEntity : parsed ingredient
     * @return Float quantity if ingredient and recipe are found
     */
    public float getIngredientQuantity(RecipeEntity recipeEntity, IngredientEntity ingredientEntity){
        return this.recipeIngredientDAL.getByRecipeEntityAndIngredientEntity(recipeEntity, ingredientEntity).getQuantity();
    }

    /**
     * Find if a jointure exists between an ingredient and a recipe
     * @param recipe_name : String recipe name
     * @param ingredient_name : String ingredient name
     * @return True if both ingredient and recipe are linked, else false
     */
    public boolean existsByName(String recipe_name, String ingredient_name) {
        return this.recipeIngredientDAL.existsByRecipeEntityNameAndIngredientEntityName(recipe_name, ingredient_name);
    }

    public ArrayList<RecipeEntity> getRecipesByIngredient(IngredientEntity ingredientEntity, RecipeService recipeService) {
        ArrayList<RecipeIngredient> entities = this.recipeIngredientDAL.getAllByIngredientEntity(ingredientEntity);
        ArrayList<RecipeEntity> recipes = new ArrayList<RecipeEntity>();
        for (RecipeIngredient entity : entities) {
            recipes.add(recipeService.findRecipeById(entity.getRecipeEntity().getId()));
        }
        return recipes;
    }

    public boolean useAllIngredients(RecipeEntity recipeEntity, String[] ingredients) {
        for (String ingredient :
                ingredients) {
            if (!this.existsByName(recipeEntity.getName(), ingredient)) {
                return false;
            }
        }
        return true;
    }

}

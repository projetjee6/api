package com.group6.projectjee.use_case.exceptions;

public class UserRoleNotDefinedException extends RuntimeException {
    public UserRoleNotDefinedException()
    {
        super(String.format("Role not defined"));
    }

}

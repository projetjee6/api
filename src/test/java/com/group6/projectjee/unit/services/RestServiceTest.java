package com.group6.projectjee.unit.services;

import com.group6.projectjee.infrastructure.persistence.entities.RecipeMealDB;
import com.group6.projectjee.use_case.services.RestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RestServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    private RestService restService;

    @Before
    public void setUp() {
        this.restService = new RestService(new RestTemplateBuilder());
    }

    @Test
    public void should_get_recipe_from_themealdb() {
        RecipeMealDB testRecipe = this.restService.getRecipe("https://www.themealdb.com/api/json/v1/1/random.php");
        assertThat(testRecipe.meals.size()).isGreaterThan(0);
    }
}

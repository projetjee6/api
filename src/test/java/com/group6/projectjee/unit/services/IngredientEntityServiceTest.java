package com.group6.projectjee.unit.services;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.use_case.services.IngredientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class IngredientEntityServiceTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IngredientDAL ingredientDAL;
    private IngredientService ingredientService;
    IngredientEntity ingredientEntity;

    @Before
    public void setUp() {
        this.ingredientService = new IngredientService(this.ingredientDAL);
        this.ingredientEntity = new IngredientEntity("testIngredient", BaseUnit.NO_UNIT);
    }

    @Test
    public void should_find_ingredient() {
        entityManager.persistAndFlush(this.ingredientEntity);

        assertThat(this.ingredientService.findByName(this.ingredientEntity.getName())).isEqualTo(this.ingredientEntity);
    }

    @Test
    public void should_create_and_check_if_exists_ingredient() {
        IngredientEntity testIngredientEntity = this.ingredientService.create(this.ingredientEntity);
        assertThat(this.ingredientService.existsById(testIngredientEntity.getId())).isEqualTo(true);
    }

    @Test
    public void should_get_all_ingredients() {
        this.ingredientService.create(this.ingredientEntity);
        assertThat(this.ingredientService.getAllIngredients().iterator().hasNext()).isEqualTo(true);
    }

}

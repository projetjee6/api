package com.group6.projectjee.unit.services;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeIngredient;
import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeIngredientDAL;
import com.group6.projectjee.use_case.services.IngredientService;
import com.group6.projectjee.use_case.services.RecipeIngredientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RecipeEntityIngredientEntityServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired private RecipeIngredientDAL recipeIngredientDAL;
    @Autowired private IngredientDAL ingredientDAL;
    private IngredientService ingredientService;
    private RecipeIngredientService recipeIngredientService;
    RecipeIngredient recipeIngredient;
    RecipeEntity testRecipeEntity;
    IngredientEntity testIngredientEntity;

    @Before
    public void setUp() {
        this.ingredientService = new IngredientService(this.ingredientDAL);
        this.recipeIngredientService = new RecipeIngredientService(this.recipeIngredientDAL, this.ingredientService);

        this.testRecipeEntity = new RecipeEntity("testRecipe");
        this.testIngredientEntity = new IngredientEntity("testIngredient", BaseUnit.NO_UNIT);
        this.recipeIngredient = new RecipeIngredient(this.testRecipeEntity, this.testIngredientEntity, 5f);
        entityManager.persistAndFlush(this.recipeIngredient);
    }

    @Test
    public void get_ingredient_quantity() {
        assertThat(this.recipeIngredientService.getIngredientQuantity(this.testRecipeEntity, this.testIngredientEntity)).isEqualTo(5f);
    }

    @Test
    public void get_ingredients_by_recipe() {
        assertThat(this.recipeIngredientService.getRecipeIngredients(this.testRecipeEntity)).contains(this.testIngredientEntity);
    }
}

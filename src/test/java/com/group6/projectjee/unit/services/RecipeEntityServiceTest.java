package com.group6.projectjee.unit.services;

import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeIngredientDAL;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeDAL;
import com.group6.projectjee.use_case.services.IngredientService;
import com.group6.projectjee.use_case.services.RecipeIngredientService;
import com.group6.projectjee.use_case.services.RecipeService;
import com.group6.projectjee.use_case.services.RestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RecipeEntityServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    private IngredientService ingredientService;
    private RecipeIngredientService recipeIngredientService;
    private RecipeService recipeService;
    private RestService restService;
    @Autowired private IngredientDAL ingredientDAL;
    @Autowired private RecipeIngredientDAL recipeIngredientDAL;
    @Autowired private RecipeDAL recipeDAL;

    public RecipeEntity testRecipeEntity;

    @Before
    public void setUp() {
        this.restService = new RestService(new RestTemplateBuilder());
        this.ingredientService = new IngredientService(this.ingredientDAL);
        this.recipeIngredientService = new RecipeIngredientService(this.recipeIngredientDAL, this.ingredientService);
        this.recipeService = new RecipeService(this.recipeDAL, this. recipeIngredientService, this.restService, ingredientService);

        this.testRecipeEntity = new RecipeEntity("testRecipe");
    }

    @Test
    public void should_create_and_find_recipe() throws Exception {
        RecipeEntity recipeEntity = this.recipeService.create(this.testRecipeEntity);

        assertThat(this.recipeService.findRecipeByName(recipeEntity.getName())).isEqualTo(this.testRecipeEntity);
        assertThat(this.recipeService.findRecipeById(recipeEntity.getId())).isEqualTo(this.testRecipeEntity);
    }

    @Test
    public void should_get_random_recipe() throws Exception {
        RecipeEntity anotherRecipeEntity = new RecipeEntity("another recipe");
        this.recipeService.create(anotherRecipeEntity);

        assertThat(this.recipeService.getRandomRecipe()).isIn(this.testRecipeEntity, anotherRecipeEntity);
    }
}

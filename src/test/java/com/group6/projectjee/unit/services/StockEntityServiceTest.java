package com.group6.projectjee.unit.services;

import com.group6.projectjee.domain.models.Stock;
import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.dal.*;
import com.group6.projectjee.infrastructure.persistence.entities.*;
import com.group6.projectjee.use_case.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StockEntityServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired private UserDAL userDAL;
    @Autowired private StockDAL stockDAL;
    @Autowired private IngredientDAL ingredientDAL;
    @Autowired private RecipeIngredientDAL recipeIngredientDAL;
    @Autowired private RecipeDAL recipeDAL;
    @Autowired private StockIngredientDAL stockIngredientDAL;
    private StockIngredientService stockIngredientService;
    private IngredientService ingredientService;
    private RecipeService recipeService;
    private RecipeIngredientService recipeIngredientService;
    private RestService restService;
    private StockService stockService;
    private StockIngredient testStockIngredient;

    private UserEntity testUserEntity;
    private StockEntity testStockEntity;
    private IngredientEntity testIngredientEntity;
    private RecipeEntity testRecipeEntity;
    private RecipeIngredient testRecipeIngredient;

    @Before
    public void setUp() {
        this.restService = new RestService(new RestTemplateBuilder());
        this.ingredientService = new IngredientService(this.ingredientDAL);
        this.stockIngredientService = new StockIngredientService(this.stockIngredientDAL, this.ingredientService);
        this.recipeIngredientService = new RecipeIngredientService(this.recipeIngredientDAL, this.ingredientService);
        this.recipeService = new RecipeService(this.recipeDAL, this. recipeIngredientService, this.restService, ingredientService);
        this.stockService = new StockService(this.userDAL, this.stockDAL, this.stockIngredientService, this.ingredientService, this.recipeService, this.recipeIngredientService);

        this.testUserEntity = new UserEntity("testFirstname", "testLastname", "testUsername", "testemail@gmail.com", "testPassword");
        this.testIngredientEntity = new IngredientEntity("testIngredient", BaseUnit.NO_UNIT);
        this.testStockEntity = new StockEntity("testStock", this.testUserEntity);
        this.testRecipeEntity = new RecipeEntity("testRecipe");
        this.testRecipeIngredient = new RecipeIngredient(this.testRecipeEntity, this.testIngredientEntity, 1.f);
        this.testStockIngredient = new StockIngredient(this.testStockEntity, this.testIngredientEntity, 8.f, 5.f);

        ArrayList<RecipeIngredient> testRecipeIngredients = new ArrayList<RecipeIngredient>();
        ArrayList<StockEntity> testStockEntityList = new ArrayList<StockEntity>();
        ArrayList<StockIngredient> testStockIngredients = new ArrayList<StockIngredient>();

        testRecipeIngredients.add(this.testRecipeIngredient);
        testStockEntityList.add(this.testStockEntity);
        testStockIngredients.add(this.testStockIngredient);

        this.testRecipeEntity.setRecipeIngredients(testRecipeIngredients);
        this.testUserEntity.setStockEntityList(testStockEntityList);
        this.testStockEntity.setStockIngredients(testStockIngredients);

        entityManager.persistAndFlush(this.testUserEntity);
        entityManager.persistAndFlush(this.testStockEntity);
        entityManager.persistAndFlush(this.testIngredientEntity);
        entityManager.persistAndFlush(this.testRecipeIngredient);
        entityManager.persistAndFlush(this.testStockIngredient);
    }

    @Test
    public void should_get_user_ingredients() {
        assertThat(this.stockService.getUserIngredients(this.testUserEntity.getId())).contains(this.testIngredientEntity);
    }

    @Test
    public void should_update_add_and_get_user_ingredient_quantity() {
        this.stockService.updateIngredientQuantity(this.testUserEntity.getId(), this.testIngredientEntity.getId(), 10.f);
        this.stockService.addIngredientQuantity(this.testUserEntity.getId(), this.testIngredientEntity.getId(), 10.f);
        assertThat(this.stockService.getUserIngredientQuantity(this.testUserEntity.getId(), this.testIngredientEntity.getId())).isEqualTo(20.f);
    }

    @Test
    public void should_update_and_get_user_ingredient_limit() {
        this.stockService.updateIngredientLimit(this.testUserEntity.getId(), this.testIngredientEntity.getId(), 12.f);
        assertThat(this.stockService.getIngredientLimit(this.testUserEntity.getId(), this.testIngredientEntity.getId())).isEqualTo(12.f);
    }

    @Test
    public void should_get_all_available_recipes_and_check_one() {
        ArrayList<RecipeEntity> recipeEntities = this.stockService.getAllAvailableRecipes(this.testUserEntity.getId());

        assertThat(recipeEntities.size()).isGreaterThan(0);
        assertThat(this.stockService.availableRecipeInternal(this.testUserEntity.getId(), recipeEntities.get(0))).isEqualTo(true);
    }
}

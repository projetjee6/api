package com.group6.projectjee.unit.services;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeDAL;
import com.group6.projectjee.infrastructure.persistence.dal.RecipeIngredientDAL;
import com.group6.projectjee.infrastructure.persistence.entities.Meal;
import com.group6.projectjee.infrastructure.persistence.entities.RecipeEntity;
import com.group6.projectjee.use_case.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MealDBServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    private IngredientService ingredientService;
    private RecipeIngredientService recipeIngredientService;
    private RecipeService recipeService;
    private MealDBService mealDBService;
    private RestService restService;
    @Autowired private IngredientDAL ingredientDAL;
    @Autowired private RecipeIngredientDAL recipeIngredientDAL;
    @Autowired private RecipeDAL recipeDAL;

    @Before
    public void setUp() {
        this.restService = new RestService(new RestTemplateBuilder());
        this.ingredientService = new IngredientService(this.ingredientDAL);
        this.recipeIngredientService = new RecipeIngredientService(this.recipeIngredientDAL, this.ingredientService);
        this.recipeService = new RecipeService(this.recipeDAL, this.recipeIngredientService, this.restService, ingredientService);
        mealDBService = new MealDBService(ingredientService, recipeIngredientService, recipeService);
    }

    @Test
    public void should_parse_unit() {
        String measure = "8 grammes";

        BaseUnit unit = mealDBService.parseUnit(measure);

        assertThat(unit).isEqualTo(BaseUnit.G);

    }

    @Test
    public void should_parse_quantity() {
        String quantity = "15 litres";

        Float parsed_quantity = mealDBService.parseQuantity(quantity);

        assertThat(parsed_quantity).isEqualTo(15.0f);
    }

    @Test
    public void should_check_ingredients_and_parse_meal_to_recipe() {
        Meal meal = new Meal(
                "testName",
                "testArea",
                "testCategory",
                "testIngredient1",
                "testIngredient2",
                "testIngredient3",
                "testIngredient4",
                "testIngredient5",
                "testIngredient6",
                "testInstructions",
                "testMealThumb",
                "1 testMeasure1",
                "1 testMeasure2",
                "1 testMeasure3",
                "1 testMeasure4",
                "1 testMeasure5",
                "1 testMeasure6"
                );

        mealDBService.checkIngredients(meal);
        try {
            RecipeEntity recipeEntity = mealDBService.toRecipe(meal);
            assertThat(meal.getStrMeal()).isEqualTo(recipeEntity.getName());
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}

package com.group6.projectjee.unit.services;

import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import com.group6.projectjee.use_case.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserEntityServiceTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired private UserDAL userDAL;
    private UserService userService;

    private UserEntity testUserEntity;

    @Before
    public void setUp() {
        this.userService = new UserService(this.userDAL);
        this.testUserEntity = new UserEntity("testFirstname", "testLastname", "testUsername", "testemail@gmail.com", "testPassword");
    }

    @Test
    public void should_create_and_find_user_by_id() {
        UserEntity userEntity = this.userService.create(this.testUserEntity);
        assertThat(this.userService.findUserById(userEntity.getId())).isNotEmpty();
    }

    @Test
    public void should_create_get_all_and_delete_user() {
        UserEntity userEntity = this.userService.create(this.testUserEntity);

        assertThat(this.userService.getAllUsers()).contains(this.testUserEntity);
        this.userService.delete(userEntity);
        assertThat(this.userService.getAllUsers()).doesNotContain(this.testUserEntity);
    }
}

package com.group6.projectjee.unit.services;

import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.entities.IngredientEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockEntity;
import com.group6.projectjee.infrastructure.persistence.entities.StockIngredient;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.infrastructure.persistence.dal.StockIngredientDAL;
import com.group6.projectjee.use_case.services.IngredientService;
import com.group6.projectjee.use_case.services.StockIngredientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StockEntityIngredientEntityServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired private IngredientDAL ingredientDAL;
    @Autowired private StockIngredientDAL stockIngredientDAL;
    private IngredientService ingredientService;
    private StockIngredientService stockIngredientService;

    private StockIngredient testStockIngredient;
    private StockEntity testStockEntity;
    private IngredientEntity testIngredientEntity;
    private UserEntity testUserEntity;

    @Before
    public void setUp() {
        this.ingredientService = new IngredientService(this.ingredientDAL);
        this.stockIngredientService = new StockIngredientService(this.stockIngredientDAL, this.ingredientService);
        this.testUserEntity = new UserEntity("testFirstname", "testLastname", "testUsername", "testemail@gmail.com", "testPassword");
        this.testIngredientEntity = new IngredientEntity("testIngredient", BaseUnit.NO_UNIT);
        this.testStockEntity = new StockEntity("testStock", this.testUserEntity);
        this.testStockIngredient = new StockIngredient(this.testStockEntity, this.testIngredientEntity, 8.f, 5.f);

        entityManager.persistAndFlush(this.testUserEntity);
        entityManager.persistAndFlush(this.testStockEntity);
        entityManager.persistAndFlush(this.testIngredientEntity);
        entityManager.persistAndFlush(this.testStockIngredient);
    }

    @Test
    public void should_check_if_exists_and_get_stock() {
        assertThat(this.stockIngredientService.existsByStock(this.testStockEntity)).isEqualTo(true);
        assertThat(this.stockIngredientService.getByStock(this.testStockEntity)).isEqualTo(this.testStockIngredient);
    }

    @Test
    public void should_update_and_get_quantity() {
        this.stockIngredientService.updateQuantity(this.testIngredientEntity, this.testStockEntity, 10.f);
        assertThat(this.stockIngredientService.getQuantity(this.testIngredientEntity, this.testStockEntity)).isEqualTo(10.f);
    }

    @Test
    public void should_update_and_get_limit() {
        this.stockIngredientService.updateLimit(this.testIngredientEntity, this.testStockEntity, 8.f);
        assertThat(this.stockIngredientService.getLimit(this.testIngredientEntity, this.testStockEntity)).isEqualTo(8.f);
    }

    @Test
    public void should_get_all_ingredient_in_stock() {
        assertThat(this.stockIngredientService.getAllIngredientFromStock(this.testStockEntity)).contains(this.testIngredientEntity);
    }
}

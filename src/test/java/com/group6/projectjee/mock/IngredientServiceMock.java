package com.group6.projectjee.mock;

import com.group6.projectjee.infrastructure.persistence.dal.IngredientDAL;
import com.group6.projectjee.use_case.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;

public class IngredientServiceMock extends IngredientService {

    @Autowired
    private IngredientDAL ingredientDAL;

    public IngredientServiceMock(IngredientDAL ingredientDAL) {
        super(ingredientDAL);
    }

    


}

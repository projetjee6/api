package com.group6.projectjee.e2e;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.group6.projectjee.domain.models.enums.ERole;
import com.group6.projectjee.infrastructure.persistence.dal.RoleDAL;
import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import com.group6.projectjee.infrastructure.persistence.entities.Role;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.use_case.services.response.JwtResponse;
import com.group6.projectjee.web.controllers.post.LoginRequest;
import com.group6.projectjee.web.controllers.post.SignupRequest;
import io.restassured.RestAssured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.Collections;

public class E2ETestUtil {
    private final ObjectMapper objectMapper;

    public E2ETestUtil() {
        this.objectMapper = new ObjectMapper();
    }

    public String connectUser(UserEntity user, RoleDAL roleDAL, UserDAL userDAL) throws JsonProcessingException {
        roleDAL.save(new Role(ERole.ROLE_ADMIN));
        SignupRequest signupRequest = new SignupRequest(user.getUsername(), user.getEmail(), user.getFirstName(), user.getLastName(), Collections.singleton("admin"), user.getPassword());
        String bodySignup = objectMapper.writeValueAsString(signupRequest);
        RestAssured.given()
                .when()
                .body(bodySignup)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post("/api/auth/signup");

        LoginRequest loginRequest = new LoginRequest(user.getUsername(), user.getPassword());
        String bodySignin = objectMapper.writeValueAsString(loginRequest);
        return RestAssured.given()
                .when()
                .body(bodySignin)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post("/api/auth/signin")
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(JwtResponse.class).getAccessToken();
    }
}

package com.group6.projectjee.e2e;

import com.group6.projectjee.web.controllers.TestController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestController.class)
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class})
@ActiveProfiles("test")
public class TestControllerIntegrationTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mvc;

    @Before
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void should_get_public_content() throws Exception {
        mvc.perform(get("/api/test"))
                .andExpect(status().isOk())
                .andExpect(content().string("Public Content."));
    }

    @Test
    public void should_get_user_content() throws Exception {
        mvc.perform(get("/api/test/user"))
                .andExpect(status().isOk())
                .andExpect(content().string("this is new User Content."));
    }

    @Test
    public void should_get_admin_board() throws Exception {
        mvc.perform(get("/api/test/admin"))
                .andExpect(status().isOk())
                .andExpect(content().string("Admin Board."));
    }

    @Test
    public void should_get_not_found() throws Exception {
        mvc.perform(get("/user"))
                .andExpect(status().isNotFound());
    }
}

package com.group6.projectjee.e2e;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.group6.projectjee.domain.models.enums.BaseUnit;
import com.group6.projectjee.infrastructure.persistence.dal.RoleDAL;
import com.group6.projectjee.infrastructure.persistence.dal.UserDAL;
import com.group6.projectjee.infrastructure.persistence.entities.UserEntity;
import com.group6.projectjee.use_case.services.response.JwtResponse;
import com.group6.projectjee.web.controllers.post.IngredientDto;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource("/application-test.properties")
public class IngredientControllerE2ETest {
    @LocalServerPort
    private int port;
    private ObjectMapper objectMapper;
    private E2ETestUtil e2ETestUtil;
    private UserEntity userTest;
    @Autowired RoleDAL roleDAL;
    @Autowired UserDAL userDAL;

    @Before
    public void init() {
        RestAssured.port = port;
        objectMapper = new ObjectMapper();
        e2ETestUtil = new E2ETestUtil();
        userTest = new UserEntity("firstName","lastName","userTest",
                "user@gmail.com",
                "passwordTest");
    }

    @Test
    public void should_register_and_login_successfully() throws Exception {
        String jwtToken = e2ETestUtil.connectUser(userTest, roleDAL, userDAL);
    }

    @Test
    public void should_post_new_ingredient() throws Exception {
        String jwtToken = e2ETestUtil.connectUser(userTest, roleDAL, userDAL);

        IngredientDto testIngredient = new IngredientDto("testIngredient", BaseUnit.NO_UNIT);
        RestAssured.given()
                .when()
                .body(objectMapper.writeValueAsString(testIngredient))
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post("/ingredient")
                .then()
                .statusCode(HttpStatus.CREATED.value());
    }

}
